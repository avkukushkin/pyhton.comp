﻿#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Alex
#
# Created:     21.08.2018
# Copyright:   (c) Alex 2011
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

from lxml import etree
from xlwt import Workbook,Style
import logging
import datetime
import hashlib
import sys

SkipHashTags=["STATUSDATE","MODIFYDATE","STATUS"]

ValiableIDs=["DGNUMBER","DCCODE","PPCODE","PTCODE","SZCODE","CZLCODE", "PCCODE", "PRCODE", "RRCODE", "CLCODE", "CSCODE","GR_CODE","CONTRACTFIELD","FIELDCODE","PG_CODGR"]
ValiableMultIDs =[("QDTN1","QDTSUB","QVAL","XXXXX"),("QD_QDN1","QD_QDTSUB","QD_QVAL","XXXXX"),("QVKL_T_QDN1","QVKL_T_QDTSUB","QVKL_T_QVAL","XXXXX"),("DPTYPE","DPSUBTYPE","DPCURRENCY","XXXXX"),("DAFID_PBD","TS_TB","TS_BRANCH","TS_SUBB"),("TEMPLATECODE","FIELDCODE","XXXX","XXXX")]

UniverseTable={
        "QVB_GROUP":("DEPOSITGROUP",[]),
        "QVB_PARGR":("DEPGROUPPARAM",[]),
        "QVB_CUSTS":("CUSTS",[]),
        "QVB_AGEREST":("AGEREST",[]),
        "QVB_CFRPL":("CFRTL",[]),
        "qvb":("DEPOSITTYPE",[]),
        "qvkl_val":("DEPOSITCURRENCY",[]),
        "V_ALG_PSBD":("AVAILABLEOPER",[]),
        "QVB_LONG":("DEPOSITRATEONPROL",[]),
        "QVB_ADDC":("ADDITIONALCOND",[]),
        "q_sbk":("Q_SBK",[]),
        "QVB_DCF":("DEDUCTBENEFFOUNDS",[]),
        "QVB_SEGMENT":("SEGMENT",[]),
        "BCH_PSBD":("BCH_PSBD",[]),
        "QVB_ACCESS":("DEPACCESSFORM",[]),
        "QVB_TSUBB":("DEPACCESSFORMSUB",[]),
        "ContractFieldPSBD":("CONTRACTFIELDPSBD",[]),
        "TemplateFieldPSBD":("TEMPLATEFIELDPSBD",[]),
        "ContractTemplatePSBD":("CONTRACTTEMPLATEPSBD",[]),
        "ContractDepositPSBD":("CONTRACTDEPOSITPSBD",[]),
        "FIELD_TDOG":("FIELDTDOG",[]),
        "qvb_date":("BREACHTIMERATE",[]),
        "dcf_tar":("INTERESTRATE",[]),
        "QVB_CET":("QVB_CET",[])}

FieldForFilterIDs={
	"qvb":("QDTN1","QDTSUB"),
	"QVB_LONG":("L_QDTN1","L_QDTSUB"),
	"BREACHTIMERATE":("qvb_date","QD_QDTSUB"),
	"ContractDepositPSBD":("DPTYPE","DPSUBTYPE"),
	"V_ALG_PSBD":("V_AVKL_QDTN1","V_AVKL_QDTSUB"),
	"QVB_CET":("ET_QDTN1","ET_QDTSUB") ,
	"BCH_PSBD":("AT_NUM","AT_NUMSUB"),
	"QVB_ACCESS":("AS_QDTN1","AS_QDTSUB"),
	"QVB_ADDC":("DC_QDTN1","DC_QDTSUB"),
	"q_sbk":("SBK_QDN1","SBK_QDTSUB"),
	"qvb_date":("QD_QDN1","QD_QDTSUB"),
	"qvkl_val":("QVKL_T_QDTN1","QVKL_T_QDTSUB"),
	"dcf_tar":("KOD_VKL_QDTN1","KOD_VKL_QDTSUB")
	}

FieldForSubFilters={"QVB_TSUBB": ("TS_ID","QVB_ACCESS","AS_ID")}

FilterIDs=[]

FilterExclIDs=[
    ('7','1'),
    ('7','2'),

    ('8','1'),
    ('8','2'),
    ('8','3'),
    ('8','4'),
    ('8','5'),


    ('50','1'),
    ('50','2'),
    ('50','3'),
    ('50','4'),
    ('50','5'),
    ('50','6'),
    ('50','7'),
    ('50','8'),
    ('50','9'),
    ('50','10'),
    ('50','11'),
    ('50','12'),
    ('50','13'),
    ('50','14'),
    ('50','15'),
    ('50','16'),
    ('50','17'),
    ('50','18'),
    ('50','19'),
    ('50','20'),
    ('50','21'),
    ('50','22'),
    ('50','23'),

    ('51','1'),
    ('51','2'),
    ('51','3'),
    ('51','4'),
    ('51','5'),
    ('51','6'),
    ('51','7'),
    ('51','8'),
    ('51','9'),
    ('51','10'),
    ('51','11'),
    ('51','12'),
    ('51','13'),
    ('51','14'),
    ('51','15'),
    ('51','16'),
    ('51','17'),
    ('51','18'),
    ('51','19'),
    ('51','20'),
    ('51','21'),
    ('51','22'),
    ('51','23'),

    ('52','1'),
    ('52','2'),
    ('52','3'),
    ('52','4'),
    ('52','5'),
    ('52','6'),
    ('52','7'),

    ('53','1'),
    ('53','2'),
    ('53','3'),

    ('54','1'),
    ('54','2'),
    ('54','3'),
    ('54','4'),
    ('54','5'),
    ('54','6'),
    ('54','7'),
    ('54','8'),

    ('55','1'),
    ('55','2'),
    ('55','3'),
    ('55','4'),

    ('56','1'),
    ('57','1'),
    ('57','2'),

    ('63','12'),
    ('63','13'),
    ('63','14'),
    ('63','15'),

    ('98','2'),

    ('99','1'),
    ('99','2'),
    ('99','3'),
    ('99','4'),
    ('99','5'),
    ('99','6'),
    ('99','7'),
    ('99','11'),
    ('99','12'),
    ('99','13'),
    ('99','14'),
    ('99','15'),
    ('99','16'),
    ('99','17'),
    ('99','40'),


    ('100','1'),
    ('100','2'),
    ('100','3'),
    ('100','4'),
    ('100','5'),
    ('100','6'),
    ('100','7'),
    ('100','8'),
    ('100','9'),
    ('100','10'),
    ('107','0')]

def ValueOr(array,name,value):
    if array.has_key(name):
        return array[name]
    else:
        return value

def GetValueByName(_str,name):
    _value=None
    pos1=_str.find(name)
    pos2=_str.find('value="',pos1)+6
    pos3=_str.find('"',pos2+1)
    pos4=_str.find("/>",pos3)
    if pos1>=0 and pos2>pos1 and pos3>pos2 and pos4>pos3:
        _value=_str[pos2+1:pos3]
    return _value

def SkipTagWithName(_str,name):
    pos1=_str.find(name)
    if pos1>=0:
        pos1=_str.rfind("<",1,pos1)
        pos2=_str.find("/>",pos1)+2
        if pos1>=0 and pos2>=0:
            _str=_str[:pos1]+_str[pos2:]
    return _str

class DataSetHeader():
    def __init__(self, xml):
        tmpxml=xml.find("rollout")
        self.dateSet=tmpxml.attrib["dateSet"]
        self.name=tmpxml.attrib["name"]
        self.timeSet=tmpxml.attrib["timeSet"]
        self.dateSet=tmpxml.attrib["dateSet"]
        self.version=tmpxml.attrib["version"]
        self.comment=xml.findtext("rollout/comment")
        logging.info('#rollout name='+self.name)

class Field():
    def __init__(self, xml, pos):
        self.pos=pos
        self.name=xml.attrib["name"]
        self.type=xml.attrib["type"]
        self.length=ValueOr(xml.attrib, "length","N/A")
        self.nullable=xml.attrib["nullable"]
        self.label=xml.attrib["label"]
        self.comment=xml.attrib["comment"]
        logging.info('#  Field:%s(%s)' % (self.name,self.type))

class Table():
    def __init__(self, xml, flagFilterIDs=False):
        self.name=xml.attrib["name"]
        self.comment=xml.findtext("comment")
        logging.info('# Table:%s' % (self.name))
        self.Fields={}
        self.Records=[]
        self.Hashes={}
        pos=0
        for f in xml.findall("tabledesc/fielddesc"):
            newField=Field(f,pos)
            self.Fields[newField.name]=newField
            pos=pos+1
        i=0
        if flagFilterIDs and FieldForFilterIDs.has_key(self.name):
            _countFiltred=0
            isTableForFiltred=True
        else:
            _countFiltred=len(xml.findall("replace/record"))
            isTableForFiltred=False
        for rec in xml.findall("replace/record"):
            _str=etree.tostring(rec).replace(" ","").replace("\t","").replace("\n","").replace("\r","")
            _isFiltered=True
            if isTableForFiltred:
                FieldType=FieldForFilterIDs[self.name][0]
                FieldSubType=FieldForFilterIDs[self.name][1]
                valType=GetValueByName(_str,FieldType)
                valSubType=GetValueByName(_str,FieldSubType)
                #_isFiltered=False
                #for val in FilterIDs:
                #    filterValType=val[0]
                #    filterValSubType=val[1]
                #    if valType==filterValType and valSubType==filterValSubType:
                #        _isFiltered=True
                #        _countFiltred=_countFiltred+1
                #        break
		_isFiltered=True
                for val in FilterExclIDs:
                    filterValType=val[0]
                    filterValSubType=val[1]
                    if valType==filterValType and valSubType==filterValSubType:
                        _isFiltered=False
                        _countFiltred=_countFiltred+1
                        break
            if not(_isFiltered):
                continue
            BusinessId=None
            for triple in ValiableMultIDs:
                if BusinessId is None:
                    name1=triple[0]
                    name2=triple[1]
                    name3=triple[2]
                    name4=triple[3]
                    value1=GetValueByName(_str,name1)
                    value2=GetValueByName(_str,name2)
                    value3=GetValueByName(_str,name3)
                    value4=GetValueByName(_str,name4)
                    if not(value1 is None):
                        BusinessId="%3s" % (value1)
                        if not(value2 is None):
                            BusinessId=BusinessId+(".%2s") % (value2)
                        if not(value3 is None):
                            BusinessId=BusinessId+(".%s1") % (value3)
                        if not(value4 is None):
                            BusinessId=BusinessId+(".%s") % (value4)
            for name in ValiableIDs:
                if BusinessId is None:
                    value=GetValueByName(_str,name)
                    if not(value is None):
                        BusinessId="%3s" % (value)
            for name in SkipHashTags:
                _str=SkipTagWithName(_str,name)
            sha = hashlib.sha1(_str).hexdigest()
            h=Hash(BusinessId,_str[:1000])
            self.Hashes[sha]=h
            i=i+1
            Values={}
            for val in rec.findall("field"):
                name=val.attrib["name"]
                null=val.attrib["null"]
                if val.attrib.has_key("value"):
                    value=val.attrib["value"]
                else:
                    value=val.text
                Values[name]=value
            self.Records.append(Values)
        pass
        logging.info('# Records %i parse' % (len(self.Records)))
        if isTableForFiltred:
            logging.info('#      id filtering (%d recs)' % (_countFiltred))

class XMLDataSet():
    def __init__(self, filename, flagFilterIDs=False):
        self.xml = etree.parse(filename)
        self.Tables={}
        self.Header=DataSetHeader(self.xml)
        for t in self.xml.findall("rollout/transaction/table"):
            newtable=Table(t, flagFilterIDs);
            self.Tables[newtable.name]=newtable
        if flagFilterIDs:
            for tableName in FieldForSubFilters:
                filterFieldName, masterTable, idFieldName = FieldForSubFilters[tableName]
                #build ids master table
                FilterMasterIDs={}
                for rec in self.Tables[masterTable].Records:
                    id=rec[idFieldName]
                    FilterMasterIDs[id]=""
                #filter Child table
                _countBeforFilter=len(self.Tables[tableName].Records)
                for i in xrange(_countBeforFilter,0,-1):
                    id=self.Tables[tableName].Records[i-1][filterFieldName]
                    if not(FilterMasterIDs.has_key(id)):
                        self.Tables[tableName].Records.pop(i-1)
                logging.info('##Filtering %d/%d recs in %s (master %s)' % (len(self.Tables[tableName].Records),_countBeforFilter,tableName,masterTable))
        logging.info('#Parsed')
    def ExportToCsv(self):
        logging.info('#Start Export')
        wb = Workbook()
        wsi = wb.add_sheet('Info')
        #try:
        pos=1
        for tableName in self.Tables:
            pos=pos+1
            wsi.row(pos).write(0,tableName)
            wsi.row(pos).write(1,'0')
            wsi.row(pos).write(2,'-')
            wsi.row(pos).write(5,self.Tables[tableName].comment)
            for fieldname in self.Tables[tableName].Fields:
                field=self.Tables[tableName].Fields[fieldname]
                pos=pos+1
                wsi.row(pos).write(0,tableName)
                wsi.row(pos).write(1,field.pos+1)
                wsi.row(pos).write(2,field.name)
                wsi.row(pos).write(3,field.length)
                wsi.row(pos).write(4,field.type)
                wsi.row(pos).write(5,field.label)
                wsi.row(pos).write(6,field.comment)
        None
        for tableName in self.Tables:
            ws = wb.add_sheet(tableName)
            logging.info('# %s...' % (tableName))
            row=0
            ws.row(row).write(0,self.Tables[tableName].comment)
            row=row+1
            col=0
            for fieldname in self.Tables[tableName].Fields:
                field=self.Tables[tableName].Fields[fieldname]
                ws.row(row).write(field.pos,field.label)
                ws.row(row+2).write(field.pos,field.name)
                col=col+1
            row=row+3
            for rec in self.Tables[tableName].Records:
                for fieldname in self.Tables[tableName].Fields:
                    field=self.Tables[tableName].Fields[fieldname]
                    val=rec[field.name]
                    if rec[field.name]!=None and len(rec[field.name])>32000:
                            val=val[:32000]+'...'
                            str='#Warning Trim value (Table:%s, Row:%s, Field:%s)' % (tableName, row-3, fieldname)
                            logging.warning(str)
                    ws.row(row).write(field.pos,val)
                row=row+1
        #except:
        #    logging.info('#ERROR')
        #finally:
	filename=self.Header.name+".xls"
        logging.info('#Saving as '+filename)
        wb.save(filename)
        logging.info('#end export')
        pass

    def LogCompare(self, set2, flagTotalCompare=True):
        logging.info('#Start Compare')
        _CompTables={}
        # Check counts
        for tableName in self.Tables:
            diff=DiffTable(tableName)
            diff.ACountRows=len(self.Tables[tableName].Records)
            if set2.Tables.has_key(tableName):
                diff.BCountRows=len(set2.Tables[tableName].Records)
            else:
                diff.MTable="Deleted"
            _CompTables[tableName]=diff
        for tableName in set2.Tables:
            diff=DiffTable(tableName)
            if not(self.Tables.has_key(tableName)):
                diff.MTable="New"
                diff.BCountRows=len(set2.Tables[tableName].Records)
                _CompTables[tableName]=diff
        # Check hashes
        for tableName in _CompTables:
            diff=_CompTables[tableName]
            if flagTotalCompare: # or diff.ACountRows!=diff.BCountRows:
                if self.Tables.has_key(tableName) and set2.Tables.has_key(tableName):
                    for hh in self.Tables[tableName].Hashes:
                            if not(set2.Tables[tableName].Hashes.has_key(hh)):
                                h=self.Tables[tableName].Hashes[hh]
                                diff.RecsModify.append("Left (%s):%s" % (h.businessId, h.strxml))
                    for hh in set2.Tables[tableName].Hashes:
                            if not(self.Tables[tableName].Hashes.has_key(hh)):
                                h=set2.Tables[tableName].Hashes[hh]
                                diff.RecsModify.append("Right(%s):%s" % (h.businessId, h.strxml))
                if self.Tables.has_key(tableName) and not(set2.Tables.has_key(tableName)):
                    for hh in self.Tables[tableName].Hashes:
                            h=self.Tables[tableName].Hashes[hh]
                            diff.RecsModify.append("Left(%s):%s" % (h.businessId, h.strxml))
                if not(self.Tables.has_key(tableName)) and set2.Tables.has_key(tableName):
                    for hh in set2.Tables[tableName].Hashes:
                            h=set2.Tables[tableName].Hashes[hh]
                            diff.RecsModify.append("Right(%s):%s" % (h.businessId, h.strxml))
                diff.RecsModify.sort()
        pass
        filename="compare_%s%s_%s%s" % (self.Header.dateSet, self.Header.timeSet,set2.Header.dateSet, set2.Header.timeSet)
        filename=filename.replace(":","").replace(".","")+".txt"
        logging.info('#Create file '+filename)
        f=open(filename,"w")
        _str="First: %s %s %s \r\n" % (self.Header.name, self.Header.dateSet, self.Header.timeSet)
        f.write(_str)
        _str="Second: %s %s %s \t\n" % (set2.Header.name, set2.Header.dateSet, set2.Header.timeSet)
        f.write(_str)
        f.write("-"*80+"\r\n")
        for tableName in _CompTables:
            f.write(_CompTables[tableName].ToStr()+"\r\n")
            for md in _CompTables[tableName].RecsModify:
                f.write("\t"*5+md+"\r\n")
                pass
            pass
        f.write("-"*80+"\r\n")
        logging.info('#End Compare')
        pass

    def ReanameUniverse(self):
        logging.info('#Start Rename')
        _CompTables={}
        # Check counts
        for tableName in UniverseTable:
            logging.info('#  Trying rename %s' % (tableName))
            if self.Tables.has_key(tableName):
                newTableName=UniverseTable[tableName][0]
                newTableName=newTableName
                tableData=self.Tables.pop(tableName)
                self.Tables[newTableName]=tableData
                logging.info('#    rename %s to %s' % (tableName,newTableName))
        logging.info('#End Reaname')

    def FilterID(self):
        logging.info('#Start filter')
        for tableName in TableFieldIDs:
            logging.info('#  Trying filter %s' % (tableName))
            #if self.Tables.has_key(tableName):

        logging.info('#End filter')

class Hash():
    def __init__(self, businessId, strxml):
        self.businessId=businessId
        self.strxml=strxml

class DiffTable():
    def __init__(self, name):
        self.tableName=name
        self.MTable='Exist'
        self.ACountRows=0
        self.BCountRows=0
        self.RecsModify=[]
    def ToStr(self):
        if self.ACountRows>self.BCountRows:
            flag='>'
        elif self.ACountRows<self.BCountRows:
            flag='<'
        else:
            flag="="
        if self.MTable!='Exist':
            flag="!"
        _str="Table: %-25s (%5s) %5d, %5d %s" % (self.tableName, self.MTable, self.ACountRows, self.BCountRows, flag)
        return _str

def main():
    logging.basicConfig(filename='parsexml.log',level=logging.DEBUG)
    logging.info('='*100)
    logging.info('#Start '+str(datetime.datetime.now()))

    #my1=XMLDataSet("1.xml", flagFilterIDs=True)
    my1=XMLDataSet("1.xml")
    my1.ExportToCsv()

    #logging.info("xml1: " + inputXML1)
    #logging.info("xml2: " + inputXML2)

    #my1.ReanameUniverse()
    
    #my2=XMLDataSet("2.xml")
    #my1.LogCompare(my2, flagTotalCompare=False)
    #my1.LogCompare(my2)
    
    logging.info('#End')
    pass


if __name__ == '__main__':
    main()
